package adorch;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.restassured.response.Response;
import factory.ServiceBuilder;
import lombok.SneakyThrows;
import org.testng.Assert;
import java.util.HashMap;
import java.util.List;


/**
 * @author ashiwani
 * @date 20/06/20
 */
public class AdOrchHelper {

    @SneakyThrows
    public static HashMap<String, String> savePlacement(String payloadparam[]){
        ServiceBuilder serviceBuilder =new ServiceBuilder("AdOrch","saveplacement",getHeaders(null,null),null,null,payloadparam);
        Response response=serviceBuilder.invoker();
        if(response.getStatusCode()==501 || response.getStatusCode()==400){
            return null;
        }
        HashMap responseMap=new ObjectMapper().readValue(response.getBody().asString(), HashMap.class);
        return responseMap;
    }

    @SneakyThrows
    public static List<String> getContextSection(String param[]){
        ServiceBuilder serviceBuilder =new ServiceBuilder("AdOrch","getcontextsection",getHeaders(null,null),null,param,null);
        Response response=serviceBuilder.invoker();
        HashMap responseMap=new ObjectMapper().readValue(response.getBody().asString(), HashMap.class);
        List data=(List) responseMap.get("data");
        return data;
    }

    @SneakyThrows
    public static List<String> getContextPage(String param[]){
        ServiceBuilder serviceBuilder =new ServiceBuilder("AdOrch","getcontextpage",getHeaders(null,null),null,param,null);
        Response response=serviceBuilder.invoker();
        HashMap responseMap=new ObjectMapper().readValue(response.getBody().asString(), HashMap.class);
        List data=(List) responseMap.get("data");
        return data;
    }

    @SneakyThrows
    public static HashMap<String, String> mappingCache(String param[]){
        ServiceBuilder serviceBuilder =new ServiceBuilder("AdOrch","cacheget",getHeaders(null,null),null,param,null);
        Response response=serviceBuilder.invoker();
        HashMap responseMap=new ObjectMapper().readValue(response.getBody().asString(), HashMap.class);
        if (Boolean.parseBoolean(responseMap.get("isKeyFound").toString()))
            return (HashMap) responseMap.get("value");
        return responseMap;
    }

    @SneakyThrows
    public static HashMap<String, String> placementZoneMapping(String payloadparam[]){
        ServiceBuilder serviceBuilder =new ServiceBuilder("AdOrch","placementmapping",getHeaders(null,null),null,null,payloadparam);
        Response response=serviceBuilder.invoker();
        if(response.getStatusCode()==501 || response.getStatusCode()==400){
            return null;
        }
        HashMap responseMap=new ObjectMapper().readValue(response.getBody().asString(), HashMap.class);
        Assert.assertEquals(responseMap.get("success"),true);
        Assert.assertEquals(responseMap.get("message"),"data saved successfully");
        return responseMap;
    }
    @SneakyThrows
    public static HashMap<String, String> placementZoneInactive(String payloadparam[]){
        ServiceBuilder serviceBuilder =new ServiceBuilder("AdOrch","editactivestatus",getHeaders(null,null),null,null,payloadparam);
        Response response=serviceBuilder.invoker();
        if(response.getStatusCode()==501 || response.getStatusCode()==400 || response.getStatusCode()==500){
            return null;
        }
        HashMap responseMap=new ObjectMapper().readValue(response.getBody().asString(), HashMap.class);
        return responseMap;
    }

    private static HashMap getHeaders(String org, String os){
        if(org==null || os ==null) {
            org = "MMT";
            os = "IOS";
        }
        HashMap hashMap=new HashMap();
        hashMap.put("authorization","DFjgkXbgFGBvebc");
        hashMap.put("user-identifier", "{\"type\":\"auth\",\"<deviceId>\":\"deviceId\",\"os\":\""+os+"\",\"osVersion\":\"<osversion>\",\"appVersion\":\"<appVersio>\",\"timeZone\":\"<timeZone>\",\"value\":\"\"}");
        hashMap.put("org", org);
        hashMap.put("usradid","2424124");
        return hashMap;
    }
    @SneakyThrows
    public static HashMap<String, String> getAd(String type, String auth, String deviceId, String org){
        HashMap hashMap=new HashMap();
        hashMap.put("authorization","DFjgkXbgFGBvebc");;
        hashMap.put("user-identifier", "{\"type\":\"\""+type+"\",\"<deviceId>\":\""+deviceId+"\",\"os\":\"os\",\"osVersion\":\"<osversion>\",\"appVersion\":\"<appVersio>\",\"timeZone\":\"<timeZone>\",\"value\":\"\""+auth+"\"}");
        hashMap.put("org", org);
        hashMap.put("usradid","2424124");
        ServiceBuilder serviceBuilder =new ServiceBuilder("AdOrch","getad",hashMap,null,null,null);
        HashMap responseMap=new ObjectMapper().readValue(serviceBuilder.invoker().getBody().asString(), HashMap.class);
        HashMap placement=(HashMap) responseMap.get("placements");
        HashMap placement1=(HashMap) placement.get("placement_1");
        return placement1;
    }
}
