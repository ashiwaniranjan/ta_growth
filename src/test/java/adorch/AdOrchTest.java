package adorch;

import annotations.CDataProvider;
import factory.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.MongoUtils;
import utils.MySqlUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ashiwani
 * @date 17/06/20
 */
public class AdOrchTest extends BaseTest {
    String section = "AutomationSection" + System.currentTimeMillis();
    Integer placemenid;

    @Test(dataProvider = "DataProviderBasedOnTest", dataProviderClass = CDataProvider.class)
    @CDataProvider.FileDetails({"sheetname::saveplacement", "filename::data/testdata/excels/adorch.xlsx"})
    public void verifyEndToEnd(HashMap<String, String> hashMap) {
        String age=hashMap.get("age");
        //Creating a placement in which always taking new context
        String payloadparamSavePlacement[] = new String[]{hashMap.get("org"), hashMap.get("platform"), hashMap.get("lob")
                , hashMap.get("page"), section};
        HashMap savePlacementMap = AdOrchHelper.savePlacement(payloadparamSavePlacement);
        Assert.assertNotNull(savePlacementMap);
        Assert.assertEquals(savePlacementMap.get("success"), true);
        placemenid = Integer.parseInt(savePlacementMap.get("placementId").toString());
        //verifying from db
        MySqlUtils mySqlUtils=new MySqlUtils("userSeviceDb");
        MongoUtils mongoUtils=new MongoUtils("tst");
        String getQuery="select * from placement where id ="+placemenid;
        List<Map<String,Object>> mapList=mySqlUtils.queryForList(getQuery);
        Assert.assertTrue(mapList.size()>0);
        Assert.assertEquals(mapList.get(0).get("org"),hashMap.get("org"));
        Assert.assertEquals(mapList.get(0).get("platform"),hashMap.get("platform"));
        Assert.assertEquals(mapList.get(0).get("lob"),hashMap.get("lob"));


        //Fetching context section of above created placement and verifying
        String pathparamgetContext[] = new String[]{hashMap.get("org"), hashMap.get("platform"), hashMap.get("lob")
                , hashMap.get("page")};
        List<String> getContextData = AdOrchHelper.getContextSection(pathparamgetContext);
        Assert.assertTrue(getContextData.contains(section));

        //Fetching context page of above created placement and verifying
        String pathparamgetparam[] = new String[]{hashMap.get("org"), hashMap.get("platform"), hashMap.get("lob")};
        List<String> getContextPage = AdOrchHelper.getContextPage(pathparamgetparam);
        Assert.assertTrue(getContextPage.contains(hashMap.get("page")));

    }


    @Test(dataProvider = "DataProviderBasedOnTest", dataProviderClass = CDataProvider.class)
    @CDataProvider.FileDetails({"sheetname::saveplacement", "filename::data/testdata/excels/adorch.xlsx"})
    public void verifyNegativeScenarioPlacementSave(HashMap<String, String> hashMap) {
        String payloadparamSavePlacement[] = new String[]{hashMap.get("org"), hashMap.get("platform"), hashMap.get("lob")
                , hashMap.get("page"), hashMap.get("section")};
        HashMap savePlacementMap = AdOrchHelper.savePlacement(payloadparamSavePlacement);
        Assert.assertNull(savePlacementMap);
    }

    @Test(dataProvider = "DataProviderBasedOnTest", dataProviderClass = CDataProvider.class)
    @CDataProvider.FileDetails({"sheetname::saveplacement", "filename::data/testdata/excels/adorch.xlsx"})
    public void verifyNegativeScenarioPlacementMap(HashMap<String, String> hashMap) {
        String payloadparamzonemapping[] = new String[]{hashMap.get("placementid"), hashMap.get("zoneid"), hashMap.get("isactive")};
        HashMap mapPlacement = AdOrchHelper.placementZoneMapping(payloadparamzonemapping);
        Assert.assertNull(mapPlacement);
        HashMap mapUpdate = AdOrchHelper.placementZoneInactive(payloadparamzonemapping);
        Assert.assertNull(mapUpdate);
    }

    @Test(dataProvider = "DataProviderBasedOnTest", dataProviderClass = CDataProvider.class)
    @CDataProvider.FileDetails({"sheetname::saveplacement", "filename::data/testdata/excels/adorch.xlsx"})
    public void verifyNegativeScenarioGetSection(HashMap<String, String> hashMap) {
        String pathparamgetContext[] = new String[]{hashMap.get("org"), hashMap.get("platform"), hashMap.get("lob")
                , hashMap.get("page")};
        List<String> getContextSection = AdOrchHelper.getContextSection(pathparamgetContext);
        Assert.assertTrue(getContextSection.size() == 0);
    }

    @Test(dataProvider = "DataProviderBasedOnTest", dataProviderClass = CDataProvider.class)
    @CDataProvider.FileDetails({"sheetname::saveplacement", "filename::data/testdata/excels/adorch.xlsx"})
    public void verifyNegativeScenarioGetPage(HashMap<String, String> hashMap) {
        String pathparamgetparam[] = new String[]{hashMap.get("org"), hashMap.get("platform"), hashMap.get("lob")};
        List<String> getContextPage = AdOrchHelper.getContextPage(pathparamgetparam);
        Assert.assertTrue(getContextPage.size() == 0);
    }

}
