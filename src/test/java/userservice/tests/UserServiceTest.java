package userservice.tests;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import userservice.dp.UserServiceDp;
import userservice.helper.UserServiceHelper;
import utils.JsonHelper;

import java.util.HashMap;

/**
 * @author ashiwani
 * @date 01/08/20
 */
public class UserServiceTest {
    UserServiceHelper userServiceHelper=new UserServiceHelper();
    SoftAssert softAssert=new SoftAssert();
    @Test(dataProvider = "createUser",dataProviderClass = UserServiceDp.class)
    public void createUser(String name,String age,String email){
        HashMap hashMap= JsonHelper.deserialize(userServiceHelper.createUser(name,age,email),HashMap.class);
        softAssert.assertEquals(hashMap.get("name"),name);
        softAssert.assertEquals(hashMap.get("age").toString(),age);
        softAssert.assertAll();
    }
}
