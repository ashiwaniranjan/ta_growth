package userservice.helper;

import com.jayway.restassured.response.Response;
import factory.ServiceBuilder;

/**
 * @author ashiwani
 * @date 01/08/20
 */
public class UserServiceHelper {

    /**
     * @param name
     * @param age
     * @param email
     * @return
     */
    public String createUser(String name,String age,String email){
        String payload[]={name,age,email};
        ServiceBuilder serviceBuilder =new ServiceBuilder("UserService","createuser",null,null,null,payload);
        return serviceBuilder.invoker().getBody().asString();
    }

    /**
     * @param username
     * @return
     */
    public String getUser(String username){
        ServiceBuilder serviceBuilder =new ServiceBuilder("UserService","getuser",null,new String[]{username});
        return serviceBuilder.invoker().getBody().asString();
    }
}
