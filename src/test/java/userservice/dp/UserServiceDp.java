package userservice.dp;

import org.testng.annotations.DataProvider;

import java.util.UUID;

/**
 * @author ashiwani
 * @date 01/08/20
 */
public class UserServiceDp {

    @DataProvider
    public Object[][] createUser(){
        String name= UUID.randomUUID().toString();
        String email=UUID.randomUUID().toString()+"@gmail.com";
        return new Object[][]{{name,"12",email}};
    }
}
