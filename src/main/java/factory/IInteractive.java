package factory;

import org.testng.Reporter;
import utils.JsonHelper;
/**
 * @author ashiwani
 * @date 15/06/20
 */
public interface IInteractive {

    static void getEnv(){
        String env = JsonHelper.getPropertiesKey("environment", new StaticData().getSettingPropertyPath());
        Reporter.log("Select env for test "+env);
    }
    void prepareRequest();

    void getResponse();

    void validateResponse();

    default void run(){
        getEnv();
        prepareRequest();
        getResponse();
        validateResponse();
    }

}
