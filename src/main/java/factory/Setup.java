package factory;

import lombok.SneakyThrows;
import utils.ExcelUtils;
import utils.JsonHelper;
import java.io.File;
import java.util.HashMap;
import java.util.List;

/**
 * @author ashiwani
 * @date 13/06/20
 */
public class Setup {
    /**
     * @param serviceName
     * @param apiname
     * @return
     */
    @SneakyThrows
    public static HashMap getServiceInfo(String serviceName, String apiname) {
        String filename = new StaticData().serviceDataPath + serviceName + ".xlsx";
        File f = new File(filename);
        if (f.exists()) {
            ExcelUtils excelUtils = new ExcelUtils(filename, serviceName);
            List<HashMap> excelHelperDataAsMap = (List) excelUtils.getDataAsMap();
            for (HashMap hashMap : excelHelperDataAsMap) {
                if (hashMap.get("SERVICENAME").toString().equalsIgnoreCase(serviceName) && hashMap.get("APINAME").toString().equalsIgnoreCase(apiname)) {
                    return hashMap;
                }
            }
        } else throw new Exception("Service details file " + serviceName + ".xlsx not exist");
        return null;
    }


    /**
     * @param serviceInfo
     * @param param
     * @return
     */

    @SneakyThrows
    public static String prepareurl(HashMap serviceInfo, String param[]) {
        String baseUrl = JsonHelper.getResourceJsonData("url",null).get(serviceInfo.get("SERVICENAME").toString());
        if (baseUrl == null) {
            throw new Exception("Baseurl is null,expected url " + serviceInfo.get("SERVICENAME").toString() + "BaseUrl");
        }
        if (serviceInfo.get("PARAMREQUIRED")==null || serviceInfo.get("PARAMREQUIRED").toString().equalsIgnoreCase("NO") || serviceInfo.get("PARAMREQUIRED").toString().equals("")) {
            return baseUrl + "/" + serviceInfo.get("ENDPOINT").toString();
        }
        else {
            return JsonHelper.jsonParameterize(baseUrl + "/" + serviceInfo.get("ENDPOINT").toString(), param);
        }
    }

    /**
     * @param serviceInfo
     * @param payload
     * @param payloadParam
     * @return
     */
    @SneakyThrows
    public static String preparePayload(HashMap serviceInfo, String payload, String payloadParam[]) {
        if (serviceInfo.get("PAYLOADPARAMREQUIRED")==null || serviceInfo.get("PAYLOADPARAMREQUIRED").toString().equalsIgnoreCase("NO") || serviceInfo.get("PAYLOADPARAMREQUIRED").toString().equalsIgnoreCase("") || serviceInfo.get("METHOD").toString().equalsIgnoreCase("GET")) {
            return payload;
        } else if (serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("BINARY")) {
            return null;// "Payload not forming as its binary format";
        } else {
            String filename = new StaticData().getPayloadParamPath() + serviceInfo.get("SERVICENAME").toString() + "_" + serviceInfo.get("APINAME").toString();
            File f = new File(filename);
            if (f.exists())
                if (JsonHelper.readFileAsString(filename).equalsIgnoreCase("") || JsonHelper.readFileAsString(filename) == null) {
                    throw new Exception("Parametrize Payload file is blank");
                } else
                    return JsonHelper.jsonParameterize(JsonHelper.readFileAsString(filename), payloadParam);
            else {
                JsonHelper.createFile(filename);
                throw new Exception("Parametrize Payload file was not exist " + serviceInfo.get("SERVICENAME").toString() + "_" + serviceInfo.get("APINAME").toString() + " created file, add data at " + filename);
            }
        }
    }
}
