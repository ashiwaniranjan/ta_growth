package factory;

import org.testng.annotations.Test;
import utils.JsonHelper;
import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendMail {
    String host = "smtp.gmail.com";
    String port = "587";
    String mailFrom = JsonHelper.getPropertiesKey("sendFrom", "/home/mmt8358/IdeaProjects/TA-GROWTH/data/settings.properties");
    String password = JsonHelper.getPropertiesKey("password", "/home/mmt8358/IdeaProjects/TA-GROWTH/data/settings.properties");
    String mailTo = JsonHelper.getPropertiesKey("sendTo", "/home/mmt8358/IdeaProjects/TA-GROWTH/data/settings.properties");
    public void sendHtmlEmail(String subject, String message) throws MessagingException {
        Properties properties = new Properties();
        properties.put("mail.smtp.host", host);
        properties.put("mail.smtp.port", port);
        properties.put("mail.smtp.auth", "true");
        properties.put("mail.smtp.starttls.enable", "true");
        Authenticator auth = new Authenticator() {
            public PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(mailFrom, password);
            }
        };
        Session session = Session.getInstance(properties, auth);
        Message msg = new MimeMessage(session);
        msg.setFrom(new InternetAddress(mailFrom));
        String[] recipientList = mailTo.split(",");
        InternetAddress[] recipientAddress = new InternetAddress[recipientList.length];
        int counter = 0;
        for (String recipient : recipientList) {
            recipientAddress[counter] = new InternetAddress(recipient.trim());
            counter++;
        }
        msg.setRecipients(Message.RecipientType.TO, recipientAddress);
        msg.setSubject(subject);
        msg.setSentDate(new Date());
        msg.setContent(message, "text/html");
        Transport.send(msg);
    }
    @Test
    private void send() {
        String subject="Test Mail";
        String message = "<i>Greetings!</i><br>";
        message += "<b>Wish you a nice day!</b><br>";
        message += "<font color=red>Duke</font>";
        try {
            sendHtmlEmail(subject, message);
            System.out.println("Email sent.");
        } catch (Exception ex) {
            System.out.println("Failed to sent email.");
            ex.printStackTrace();
        }
    }
}

