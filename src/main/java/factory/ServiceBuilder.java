package factory;


import com.jayway.restassured.response.Response;
import org.testng.Reporter;

import java.util.HashMap;
/**
 * @author ashiwani
 * @date 13/06/20
 */
public class ServiceBuilder {
    String serviceName;
    String apiName;
    String payload;
    HashMap<String, String> headers;
    String queryParam[];
    String payloadParam[];
    Byte body[];//Its for handeling binary payload for grpc services

    public ServiceBuilder(String serviceName, String apiName, HashMap<String, String> headers,String queryParam[]) {
        this.serviceName = serviceName;
        this.apiName = apiName;
        this.headers = headers;
        this.queryParam = queryParam;
    }

    public ServiceBuilder(String serviceName, String apiName, HashMap<String, String> headers, String payload, String queryParam[], String payloadParam[]) {
        this.serviceName = serviceName;
        this.apiName = apiName;
        this.payload = payload;
        this.queryParam = queryParam;
        this.payloadParam=payloadParam;
        this.headers = headers;
    }

    public ServiceBuilder(String serviceName, String apiName, HashMap<String, String> headers, String payload, String payloadParam[]) {
        this.serviceName = serviceName;
        this.apiName = apiName;
        this.payload = payload;
        this.payloadParam=payloadParam;
        this.headers = headers;
    }

    public ServiceBuilder(String serviceName, String apiName, HashMap<String, String> headers, Byte body[]) {
        this.serviceName = serviceName;
        this.apiName = apiName;
        this.body = body;
        this.headers = headers;
    }

    @lombok.SneakyThrows
    public Response invoker() {
        HashMap serviceInfo = Setup.getServiceInfo(serviceName,apiName);
        if(serviceInfo==null)
            throw new Exception("Service apis details not exist");
        Reporter.log("<****************TEST START******************>",true);
        Reporter.log("Service Name: "+serviceInfo.get("SERVICENAME"),true);
        Reporter.log("Api Name: "+serviceInfo.get("APINAME"),true);
        String url = Setup.prepareurl(serviceInfo, queryParam);
        String payloadFinal= Setup.preparePayload(serviceInfo,payload,payloadParam);
        String method=serviceInfo.get("METHOD").toString();
        switch (method) {
            case "GET":
                return RestCalls.invokeGet(url, headers);
            case "POST":
                return RestCalls.invokePost(url, payloadFinal, headers,serviceInfo,body);
            case "PUT":
                return RestCalls.invokePut(url, payloadFinal, headers,serviceInfo,body);
            case "DELETE":
                return RestCalls.invokeDelete(url, payloadFinal, headers,serviceInfo,body);
            default:
                throw new Exception(method+" METHOD NOT SUPPORTED");
        }
    }
}
