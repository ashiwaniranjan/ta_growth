package factory;

/**
 * @author ashiwani
 * @date 13/06/20
 */
public class StaticData {
    public String payloadParamPath = "data/testdata/json/";
    public String serviceDataPath = "data/apis/";
    public String settingPropertyPath = "data/settings.properties";
    public String environmentPath = "data/environment/";
    public String getPayloadParamPath() {
        return payloadParamPath;
    }

    public void setPayloadParamPath(String payloadParamPath) {
        this.payloadParamPath = payloadParamPath;
    }

    public String getServiceDataPath() {
        return serviceDataPath;
    }

    public void setServiceDataPath(String serviceDataPath) {
        this.serviceDataPath = serviceDataPath;
    }

    public String getSettingPropertyPath() {
        return settingPropertyPath;
    }

    public void setSettingPropertyPath(String settingPropertyPath) {
        this.settingPropertyPath = settingPropertyPath;
    }

    public String getEnvironmentPath() {
        return environmentPath;
    }

    public void setEnvironmentPath(String environmentPath) {
        this.environmentPath = environmentPath;
    }

}
