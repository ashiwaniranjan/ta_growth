package factory;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.testng.asserts.IAssert;
import org.testng.asserts.SoftAssert;

/**
 * @author ashiwani
 * @date 13/06/20
 */
public class CAssert extends SoftAssert implements ITestListener {

    @Override
    public void executeAssert(IAssert a) {
        super.executeAssert(a);
    }

    @Override
    public void assertAll() {
        super.assertAll();
    }

    @Override
    protected void doAssert(IAssert assertCommand) {
        super.doAssert(assertCommand);
    }

    @Override
    public void onAssertSuccess(IAssert assertCommand) {
        super.onAssertSuccess(assertCommand);
    }

    @Override
    public void onAssertFailure(IAssert assertCommand) {
        super.onAssertFailure(assertCommand);
    }

    @Override
    public void onAssertFailure(IAssert assertCommand, AssertionError ex) {

        super.onAssertFailure(assertCommand, ex);
    }

    @Override
    public void onBeforeAssert(IAssert assertCommand) {
        super.onBeforeAssert(assertCommand);
    }

    @Override
    public void onAfterAssert(IAssert assertCommand) {
        super.onAfterAssert(assertCommand);
    }

    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println("jjjj,"+iTestResult.getHost());
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        System.out.println(iTestContext.getHost());

    }

    @Override
    public void onFinish(ITestContext iTestContext) {

    }
}