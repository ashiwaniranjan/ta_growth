package factory;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import com.jayway.restassured.specification.RequestSpecification;
import org.testng.Reporter;

import java.util.Calendar;
import java.util.HashMap;

/**
 * @author ashiwani
 * @date 14/06/20
 */
public class RestCalls {
    static synchronized Response invokeGet(String url, HashMap headers){
        long startTime = Calendar.getInstance().getTimeInMillis();
        RequestSpecification requestSpecification= RestAssured.given();
        if(headers!=null)
            requestSpecification.headers(headers);
        requestSpecification.contentType(ContentType.JSON);
        Response response=requestSpecification.get(url);
        long endTime = Calendar.getInstance().getTimeInMillis();
        double timeTaken = (endTime - startTime);
        log(response,url,null,"GET",headers, timeTaken);
        return response;
    }

    static  Response invokePost(String url, String payload, HashMap headers, HashMap serviceInfo, Byte body[]){
        long startTime = Calendar.getInstance().getTimeInMillis();
        RequestSpecification requestSpecification= RestAssured.given();
        if(headers!=null)
            requestSpecification.headers(headers);
        if(serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("JSON")||serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("")){
            requestSpecification.contentType(ContentType.JSON);
            requestSpecification.body(payload);
        }
        else if (serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("BINARY")){
            requestSpecification.contentType(ContentType.BINARY);
            requestSpecification.body(body);
        }
        Response response=requestSpecification.post(url);
        long endTime = Calendar.getInstance().getTimeInMillis();
        double timeTaken = (endTime - startTime);
        log(response,url,payload,"POST",headers, timeTaken);
        return response;
    }

    static Response invokeDelete(String url, String payload, HashMap headers, HashMap serviceInfo, Byte body[]){
        long startTime = Calendar.getInstance().getTimeInMillis();
        RequestSpecification requestSpecification= RestAssured.given();
        if(headers!=null)
            requestSpecification.headers(headers);
        if(serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("JSON")||serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("")){
            requestSpecification.contentType(ContentType.JSON);
            requestSpecification.body(payload);
        }
        else if (serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("BINARY")){
            requestSpecification.contentType(ContentType.BINARY);
            requestSpecification.body(body);
        }
        Response response=requestSpecification.delete(url);
        long endTime = Calendar.getInstance().getTimeInMillis();
        double timeTaken = (endTime - startTime);
        log(response,url,payload,"DELETE",headers, timeTaken);
        return response;
    }

    static Response invokePut(String url, String payload, HashMap headers, HashMap serviceInfo, Byte body[]){
        long startTime = Calendar.getInstance().getTimeInMillis();
        RequestSpecification requestSpecification= RestAssured.given();
        if(headers!=null)
            requestSpecification.headers(headers);
        if(serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("JSON")||serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("")){
            requestSpecification.contentType(ContentType.JSON);
            requestSpecification.body(payload);
        }
        else if (serviceInfo.get("PAYLOADTYPE").toString().equalsIgnoreCase("BINARY")){
            requestSpecification.contentType(ContentType.BINARY);
            requestSpecification.body(body);
        }
        Response response=requestSpecification.put(url);
        long endTime = Calendar.getInstance().getTimeInMillis();
        double timeTaken = (endTime - startTime);
        log(response,url,payload,"PUT",headers, timeTaken);
        return response;
    }
    public static void log(Response rs, String url, String payload, String method, HashMap hashMap, Double timeTaken){
        Reporter.log("Request Path: "+url,true);
        Reporter.log("Request Payload: "+payload,true);
        Reporter.log("Request Method: "+method,true);
        Reporter.log("Request Headers: "+hashMap,true);
        Reporter.log("Response Body: ",true);
        rs.then().log().body();
        Reporter.log("Response Status: ",true);
        rs.then().log().status();
        Reporter.log("Response Time: "+timeTaken/1000.0+" Sec",true);
        Reporter.log("Response Header: "+rs.getHeaders().toString(),true);
        
        Reporter.log("<****************TEST END********************>",true);

    }

}
