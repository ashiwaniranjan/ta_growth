package utils;

import com.datastax.driver.core.*;
import org.testng.Reporter;
import java.util.HashMap;
import java.util.List;

public class CassandraUtils {
    static HashMap<String,String> configmap;
    private static Session session;

    public CassandraUtils(String name) {
        configmap = JsonHelper.getResourceJsonData("cassandra",name);
    }

    public Cluster connectWithCassandra() {
        SocketOptions socketOptions = new SocketOptions();
        socketOptions.setReadTimeoutMillis(10000);
        socketOptions.setConnectTimeoutMillis(10000);
        return Cluster.builder().addContactPoints(configmap.get("host"))
                .withCredentials(configmap.get("username"), configmap.get("password"))
                .withSocketOptions(socketOptions).withProtocolVersion(ProtocolVersion.V3)
                .build();
    }
    private Session getSession() {
        if(session==null) {
            session = connectWithCassandra().connect();
            Reporter.log("New Cassandra Session is started", true);
            return session;
        }
        return session;
    }
    public List<Row> getAllRow(String query){
        ResultSet rows=getSession().execute(query);
        return rows.all();
    }

    public Row getRow(String query){
        ResultSet rows=getSession().execute(query);
        return rows.one();
    }

    public void closeSession(){
        session.close();
        Reporter.log("New Cassandra Session is closed",true);
    }


}
