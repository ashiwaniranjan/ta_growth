package utils;

import com.google.gson.Gson;
import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.geojson.Geometry;
import com.mongodb.client.model.geojson.MultiPolygon;
import com.mongodb.client.model.geojson.Point;
import com.mongodb.client.model.geojson.Position;
import org.bson.Document;
import org.bson.conversions.Bson;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import static com.mongodb.client.model.Projections.*;

/**
 * @author ashiwani
 * @date 13/06/20
 */
public class MongoUtils {
    static MongoClient mongoClient;
    static HashMap<String,String> hashMap;

    public MongoUtils(String dbname) {
        hashMap = JsonHelper.getResourceJsonData("mongo",dbname);
    }
    public static MongoClient getSharedMongoClient() {

        try {
            if (mongoClient == null) {
                ServerAddress address = new ServerAddress(hashMap.get("host"), Integer.parseInt(hashMap.get("port")));
                MongoClientOptions settings = MongoClientOptions.builder().codecRegistry(com.mongodb.MongoClient.getDefaultCodecRegistry()).build();
                List<MongoCredential> credentialList = new ArrayList<>(1);
                MongoCredential credential = MongoCredential.createCredential(hashMap.get("username"), hashMap.get("auth_db"), hashMap.get("password").toCharArray());
                credentialList.add(credential);
                mongoClient = new MongoClient(address, credentialList, settings);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mongoClient;
    }


    /**
     * close mongo connection
     */
    public void closeMongoConnection() {
        mongoClient.close();
    }

    /**
     * show all document present in mongo
     *
     * @param collection
     * @param query
     * @return
     */
    public Document showAllDocuments(MongoCollection<Document> collection, DBObject query) {

        for (Document doc : collection.find((Bson) query)) {
            return doc;
        }
        return null;
    }

    /**
     * delete all document from mongo which matches the query
     *
     * @param collection
     * @param query
     */
    public  void deleteDocuments(MongoCollection<Document> collection, Bson query) {
        collection.deleteMany(query);
    }

    public  void deleteDocuments(String db, String collection, Bson query) {
        getSharedMongoClient().getDatabase(db).getCollection(collection).deleteOne(query);
    }

    public  void deleteDocumentbyId(String db, String collection, String id) {
        getSharedMongoClient().getDatabase(db).getCollection(collection).deleteOne(Filters.eq("_id", id));
    }


    public  FindIterable<Document> getDocuments(MongoCollection<Document> collection, DBObject query, List<String> fieldtoExtract) {
        Bson[] projectbson = new Bson[fieldtoExtract.size()];
        for (int loop = 0; loop < fieldtoExtract.size(); loop++) {
            projectbson[loop] = include(fieldtoExtract.get(loop));
        }
        return collection.find((Bson) query).projection(fields(projectbson));

    }

    public  ArrayList<Document> getDocuments(String db, String collection, Bson filter, List<String> fieldNames) {
        ArrayList<Document> result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(filter).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public  ArrayList<Document> getDocuments(String db, String collection, Bson filter) {
        ArrayList result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(filter).iterator().forEachRemaining(result::add);
        return result;
    }

    public  ArrayList withinLocation(String db, String collection, String fieldName, double latitude, double longitude, List<String> fieldNames, Class classToMap) {
        ArrayList result = new ArrayList<>();
        Point hotelPoint = new Point(new Position(longitude, latitude));
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.geoIntersects(fieldName, hotelPoint)).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public  ArrayList withinLocation(String db, String collection, String fieldName, double latitude, double longitude, List<String> fieldNames, Class classToMap, Bson query) {
        ArrayList result = new ArrayList<>();
        Point hotelPoint = new Point(new Position(longitude, latitude));
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.and(Filters.in("active_status.status", "ACTIVE"), Filters.geoIntersects(fieldName, hotelPoint))).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public  ArrayList withinLocation(String db, String collection, MultiPolygon polygon, List<String> fieldNames, Class classToMap) {
        ArrayList result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.geoWithin("boundary", polygon)).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public ArrayList withinLocation(String db, String collection, String field, Bson polygon, List<String> fieldNames, Class classToMap) {
        ArrayList result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.geoWithin(field, polygon)).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public ArrayList withinLocation(String db, String collection, String field, Bson polygon, List<String> fieldNames) {
        ArrayList result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.geoWithin(field, polygon)).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    //
    public  ArrayList intersectLocation(String db, String collection, String field, Bson polygon, List<String> fieldNames) {
        ArrayList result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.geoIntersects(field, polygon)).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public  ArrayList intersectLocation(String db, String collection, String field, Geometry polygon, List<String> fieldNames) {
        ArrayList result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.geoIntersects(field, polygon)).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public static ArrayList intersectLocation(String db, String collection, String field, Bson polygon, Bson query, List<String> fieldNames) {
        ArrayList result = new ArrayList<>();
        getSharedMongoClient().getDatabase(db).getCollection(collection).find(Filters.and(Filters.geoIntersects(field, polygon), query)).projection(fields(include(fieldNames))).iterator().forEachRemaining(result::add);
        return result;
    }

    public  void insertDocuments(String db, String collection, String mongoDoc) {
        Gson gson = new Gson();
        Document doc = Document.parse(mongoDoc);
        getSharedMongoClient().getDatabase(db).getCollection(collection).insertOne(doc);
    }
        public  ArrayList nearLocation(String db, String collection, double mindistance , double maxdistance, double latitude, double longitude, Bson query, String key, int limit) {

        ArrayList result = new ArrayList<>();

        DBObject geoNearFields = new BasicDBObject();
        Point geoPoint = new Point(new Position(longitude, latitude));
        geoNearFields.put("near", geoPoint);
        geoNearFields.put("distanceField", "distance");
        geoNearFields.put("minDistance", mindistance);
        geoNearFields.put("maxDistance", maxdistance);
        geoNearFields.put("spherical", true);

        if (key != null) {
            geoNearFields.put("key", key);
        }

        if (query != null) {
            Bson filterQuery = query.toBsonDocument(Document.class, com.mongodb.MongoClient.getDefaultCodecRegistry());
            geoNearFields.put("query", filterQuery);

        }
        if(limit==0)
            geoNearFields.put("num", 100000);
        else
            geoNearFields.put("num", limit);

        BasicDBObject geoNear = new BasicDBObject("$geoNear", geoNearFields);

        getSharedMongoClient().getDatabase(db).getCollection(collection).aggregate(Arrays.asList(geoNear)).iterator().forEachRemaining(result::add);
        return result;
    }

    public  ArrayList nearLocation(String db, String collection, double distance, double latitude, double longitude, String key) {
        return nearLocation(db, collection,0, distance, latitude, longitude, null, key,0);
    }


    public  ArrayList nearLocation(String db, String collection, double distance, double latitude, double longitude) {
        return nearLocation(db, collection, 0,distance, latitude, longitude, null, null,0);
    }

    public DBObject getSingleDocument(String db, String collection) {
        return getSharedMongoClient().getDB(db).getCollection(collection).findOne();
    }

    /**
     * Insert a json in mongo collection
     * @param db
     * @param collection
     * @param json
     */

    public void insertDocument(String db, String collection, String json) {
        getSharedMongoClient().getDatabase(db).getCollection(collection).insertOne(Document.parse(json));
    }

}
