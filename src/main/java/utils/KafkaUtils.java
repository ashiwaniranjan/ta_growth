package utils;


import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;


public class KafkaUtils {
    KafkaProducer<String, byte[]> stringKafkaProducer;
    static HashMap<String,String> configmap;

    public KafkaUtils(String name) {
        configmap = JsonHelper.getResourceJsonData("kafka",name);
        Properties config = new Properties();
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, configmap.get("host")+":"+configmap.get("port"));
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class.getName());
        stringKafkaProducer = new KafkaProducer<>(config);
    }

    public void produceKafkaMsg(String topic, String message, String key) {
        byte[] messageBytes = message.getBytes();
        try {
            ProducerRecord<String, byte[]> record = new ProducerRecord<String, byte[]>(topic, key, messageBytes);
            stringKafkaProducer.send(record);
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            closeKafkaProducer();
        }
    }

    public void closeKafkaProducer() {
        stringKafkaProducer.close();
    }


    public void produceKafkaMsgs(String topic, ArrayList<String> kafkaPackets) {
        try {
            kafkaPackets.forEach(packet->{
                produceKafkaMsg(topic, packet, String.valueOf(packet.hashCode()));
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
      finally {
            closeKafkaProducer();
        }
    }
}
