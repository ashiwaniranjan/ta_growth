package utils;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ashiwani
 * @date 23/06/20
 */
public class MySqlUtils {
    String dbName;
    JdbcTemplate jdbcTemplate;

    public MySqlUtils(String dbName){
        this.dbName=dbName;
        this.jdbcTemplate=new JdbcTemplate(setConnection(dbName));
    }
    private DriverManagerDataSource setConnection(String dbame){
        HashMap<String,String> hashMap=JsonHelper.getResourceJsonData("mysql",dbame);
        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setUrl(hashMap.get("url"));
        driverManagerDataSource.setUsername(hashMap.get("username"));
        driverManagerDataSource.setPassword(hashMap.get("password"));
        driverManagerDataSource.setDriverClassName(hashMap.get("drivername"));
        return driverManagerDataSource;
    }

    @SneakyThrows
    public List<Map<String, Object>> queryForList(String query){
        try {
            List<Map<String, Object>> queryForList = jdbcTemplate.queryForList(query);
            return queryForList;
        }
        catch (Exception e){
            e.getMessage();
        }
        return null;
    }

    public int update(String query){
        int rowsUpdated = this.jdbcTemplate.update(query);
        try {
            this.jdbcTemplate.getDataSource().getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rowsUpdated;
    }

    public void execute(String sql){
        this.jdbcTemplate.execute(sql);
        try {
            this.jdbcTemplate.getDataSource().getConnection().close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
