package utils;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.policy.WritePolicy;
import com.aerospike.client.query.Filter;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;
import java.util.HashMap;
import java.util.Map;
/**
 * @author ashiwani
 * @date 14/06/20
 */
public class AerospikeUtils {

    static AerospikeClient aerospikeClient;
    static HashMap<String,String> hashMap;

    public AerospikeUtils(String name) {
        hashMap = JsonHelper.getResourceJsonData("aerospike",name);
    }

    public static AerospikeClient getSharedClient() {
        try {
            if (aerospikeClient == null) {
                aerospikeClient = new AerospikeClient(hashMap.get("host"), Integer.parseInt(hashMap.get("port")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return aerospikeClient;
    }

    /**
     * @param namespace
     * @param set
     * @param key
     * @return
     */
    public Map<String, Object> fetchRecord(String namespace, String set, String key) {
        Record record = getSharedClient().get(null, new Key(namespace, set, key));
        return record.bins;
    }

    /**
     * @param namespace
     * @param set
     * @param key
     * @param bins
     */
    public  void updateRecord(String namespace, String set, String key, Bin... bins) {
        getSharedClient().put(new WritePolicy(), new Key(namespace, set, key), bins);
    }

    /**
     * @param namespace
     * @param set
     * @param key
     * @param bins
     */
    public  void insertRecord(String namespace, String set, String key, Bin... bins) {
        getSharedClient().put(new WritePolicy(), new Key(namespace, set, key), bins);
    }


    /**
     * @param namespace
     * @param set
     * @param key
     * @return
     */
    public static void deleteRecord(String namespace, String set, String key) {
        getSharedClient().delete(new WritePolicy(), new Key(namespace, set, key));
    }

    /**
     * @param namespace
     * @param set
     * @param bin
     * @param radius
     * @param longitude
     * @param latitude
     * @return
     */
    private  Record nearBy(String namespace, String set, String bin, double radius, double longitude, double latitude) {
        Statement stmt = new Statement();
        stmt.setNamespace(namespace);
        stmt.setSetName(set);
        stmt.setFilter(Filter.geoWithinRadius(bin, longitude, latitude, radius));
        RecordSet rs = getSharedClient().query(null, stmt);
        return rs.getRecord();
    }
}




