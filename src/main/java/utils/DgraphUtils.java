package utils;

import io.dgraph.DgraphClient;
import io.dgraph.DgraphGrpc;
import io.dgraph.DgraphProto;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author ashiwani
 * @date 14/06/20
 */
public class DgraphUtils {

    static DgraphClient dgraphClient;
    static HashMap<String,String> hashMap;

    public DgraphUtils(String name) {
        hashMap = JsonHelper.getResourceJsonData("dgraph",name);
        getClient(hashMap);
    }

    private void getClient(HashMap<String,String> config) {
        ManagedChannel channel1 = ManagedChannelBuilder
                .forAddress(config.get("host"), Integer.parseInt(config.get("port")))
                .usePlaintext().build();
        DgraphGrpc.DgraphStub stub = DgraphGrpc.newStub(channel1);
        this.dgraphClient = new DgraphClient(stub);
    }
}




