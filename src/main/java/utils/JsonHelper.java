package utils;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.MapDifference;
import com.google.common.collect.Maps;
import com.google.protobuf.Message;
import com.googlecode.protobuf.format.JsonFormat;
import com.jayway.jsonpath.JsonPath;
import factory.StaticData;
import org.apache.commons.io.IOUtils;
import org.apache.commons.text.StringSubstitutor;
import org.testng.Reporter;
import java.io.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
/**
 * @author ashiwani
 * @date 13/06/20
 */
public class JsonHelper {

    /**
     * @param jsonMessage
     * @param path
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T getDataFromJson(String jsonMessage, String path, Class<T> clazz) {
        try {
            return JsonPath.parse(jsonMessage).read(path, clazz);
        } catch (Exception e) {
            Reporter.log("Exception in getting data using json Path " + e.getCause(), false);
            return null;
        }
    }

    /**
     *
     * @param object
     * @param <T>
     * @return
     */

    public <T> String serialize(T object) {
        try {
            return new ObjectMapper().writeValueAsString(object);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot serialize given object");
        }
    }

    /**
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */

    public static  <T> T deserialize(String json, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(json, clazz);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot deserialize given object");
        }
    }

    /**
     *
     * @param json
     * @param type
     * @param <T>
     * @return
     */

    public <T> T deserialize(String json, TypeReference<T> type) {
        try {
            return new ObjectMapper().readValue(json, type);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot deserialize given object");
        }
    }

    /**
     *
     * @param object
     * @param <T>
     * @return
     */

    public static  <T> String serializeProto(T object){
        return new JsonFormat().printToString((Message) object);
    }

    /**
     *
     * @param object
     * @param clazz
     * @param <T>
     * @return
     */

    public static  <T> T deserializeProto(T object, Class<T> clazz) {
        try {
            return new ObjectMapper().readValue(new JsonFormat().printToString((Message) object), clazz);
        } catch (IOException var) {
            throw new IllegalArgumentException("Cannot deserialize given object");
        }
    }

    /**
     *
     * @param PayloadRawString
     * @param StringParameters
     * @return
     */

    public static String jsonParameterize(String PayloadRawString, String[] StringParameters){
        if(StringParameters==null)
            return PayloadRawString;
        Map<String, String> valuesMap = new HashMap<String, String>();
        int paramnumber = 0;
        for (String param : StringParameters) {
            valuesMap.put(Integer.toString(paramnumber), param);
            paramnumber++;
        }
        String templateString = PayloadRawString;
        StringSubstitutor sub = new StringSubstitutor(valuesMap);
        String resolvedString = sub.replace(templateString);
        return resolvedString;
    }

    /**
     *
     * @param filepath
     * @return
     */
    public static HashMap readJsonFileReturnMap(String filepath){
        try {
            HashMap hashMap=new ObjectMapper().readValue(new File(filepath),HashMap.class);
            return hashMap;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    /**
     *
     * @param propertyName
     * @param fileName
     * @return
     */

    public static String getPropertiesKey(String propertyName, String fileName) {
        try {
            File file = new File(fileName);
            FileInputStream configFis=new FileInputStream(file);
            Properties configProp = new Properties();
            configProp.load(configFis);
            String filedetails = configProp.getProperty(propertyName);
            return filedetails;
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     *
     * @param filePath
     * @return
     * @throws IOException
     */
    public static String readFileAsString(String filePath) throws IOException
    {
        BufferedReader reader = new BufferedReader(new FileReader(filePath));
        return IOUtils.toString(reader);
    }

    /**
     *
     * @param path
     * @return
     */
    public static String createFile(String path){
        try {
            new File(path).createNewFile();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        return path;
    }

    /**
     *
     * @param type
     * @param name
     * @return
     */
    public static HashMap<String,String> getResourceJsonData(String type,String name){
        String env = getPropertiesKey("environment", new StaticData().getSettingPropertyPath());
        if(type.equalsIgnoreCase("url")){
            HashMap<String,String> typedata = (HashMap) JsonHelper.readJsonFileReturnMap(new StaticData().getEnvironmentPath() + env+ ".json").get(type.toLowerCase());
            return typedata;
        }
        else {
            List<HashMap<String, String>> typedata = (List<HashMap<String, String>>) JsonHelper.readJsonFileReturnMap(new StaticData().getEnvironmentPath() + env + ".json").get(type.toLowerCase());
            for (HashMap hashMap : typedata) {
                if (hashMap.get("name").equals(name))
                    return hashMap;
            }
        }
        return null;
    }
    /**
     *
     * @param leftJson
     * @param rightJson
     */
    public MapDifference findJsonDiff(String leftJson, String rightJson){
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<Map<String, Object>> type = new TypeReference<Map<String, Object>>() {};
        try {
            Map<String, Object> leftMap = mapper.readValue(leftJson, type);
            Map<String, Object> rightMap = mapper.readValue(rightJson, type);
            Map<String, Object> leftFlatMap = FlatMapUtils.flatten(leftMap);
            Map<String, Object> rightFlatMap = FlatMapUtils.flatten(rightMap);
            MapDifference<String, Object> difference = Maps.difference(leftFlatMap, rightFlatMap);
            return difference;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /***
     *
     * @param leftJson
     * @param rightJson
     * @param ignoreField
     * @return
     */
    public static MapDifference findJsonDiff(String leftJson,String rightJson,String ignoreField[]){
        ObjectMapper mapper = new ObjectMapper();
        TypeReference<Map<String, Object>> type = new TypeReference<Map<String, Object>>() {};
        try {
            Map<String, Object> leftMap = mapper.readValue(leftJson, type);
            Map<String, Object> rightMap = mapper.readValue(rightJson, type);
            Map<String, Object> leftFlatMap = FlatMapUtils.flatten(leftMap);
            Map<String, Object> rightFlatMap = FlatMapUtils.flatten(rightMap);
            if(ignoreField!=null | ignoreField.length!=0) {
                for (String ignore : ignoreField) {
                    rightFlatMap.remove(ignore);
                    leftFlatMap.remove(ignore);
                }
            }
            Reporter.log(leftFlatMap.toString(),false);
            Reporter.log(rightFlatMap.toString(),false);
            MapDifference<String, Object> difference = Maps.difference(leftFlatMap, rightFlatMap);
            return difference;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}