package utils;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.*;

/**
 * @author ashiwani
 * @date 13/06/20
 */
public class ExcelUtils {
    String filePath;
    String sheetName;
    FileInputStream excelFile;
    XSSFWorkbook excelWBook;
    XSSFSheet excelWSheet;
    XSSFRow row;
    XSSFCell cell;

    public ExcelUtils(String filePath, String sheetName) throws IOException {
        this.filePath=filePath;
        this.sheetName=sheetName;
        this.excelFile= new FileInputStream(filePath);
        this.excelWBook=new XSSFWorkbook(excelFile);
        this.excelWSheet=excelWBook.getSheet(sheetName);
    }

    public Object[][] getTableArray() throws Exception {

        String[][] tabArray = null;

            int startRow = 1;

            int startCol = 0;

            int ci, cj;

            int totalRows = excelWSheet.getLastRowNum();
            // you can write a function as well to get Column count

            int totalCols = excelWSheet.getRow(0).getLastCellNum();

            tabArray = new String[totalRows][totalCols];

            ci = 0;

            for (int i = startRow; i <= totalRows; i++, ci++) {

                cj = 0;

                for (int j = startCol; j < totalCols; j++, cj++) {

                    tabArray[ci][cj] = getCellData(i, j);
                }

            }



        return (tabArray);

    }

    public String getCellData(int RowNum, int ColNum) throws Exception {

        try {

            Cell cell = excelWSheet.getRow(RowNum).getCell(ColNum);

            if (cell != null) {
                cell.setCellType(Cell.CELL_TYPE_STRING);
                String CellData = cell.getStringCellValue();
                return CellData;

            }
        } catch (Exception e) {

            e.printStackTrace();
            throw (e);
        }
        return "";
    }
    public void close() throws IOException {
        excelFile.close();
    }

    public List<Map> getDataAsMap() throws IOException {

       List<Map> result = new ArrayList<>();
       List header = new ArrayList();
       excelWSheet.getRow(0).cellIterator().forEachRemaining(cell->header.add(cell.getStringCellValue()));
       excelWSheet.iterator().forEachRemaining(row -> {
           Map rowMap=new LinkedHashMap();
           for (int i = 0; i < header.size(); i++) {
               rowMap.put(header.get(i),row.getCell(i));
           }
           result.add(rowMap);
                });
        return result.subList(1,result.size()); //Do not return first row as it is a header

    }
    public  Object[][] getDataBasedOnTestMethod(Method method) throws Exception {
        int totalRows = excelWSheet.getLastRowNum();
        String testName = method.getName();
        int testCaseRowNum;
        for (testCaseRowNum = 1; testCaseRowNum <= totalRows; testCaseRowNum++) {
            String testCaseNameInExcelFile = getCellDataNew(testCaseRowNum, 0);
            if (testCaseNameInExcelFile.equalsIgnoreCase(testName)) {
                break;
            }
        }
        int dataStartRowNum = testCaseRowNum + 2;
        int testRows = 0;
        while (!getCellDataNew(dataStartRowNum + testRows,0).equalsIgnoreCase("endOfTestData")) {
            testRows++;
        }
        int colStartColNum = testCaseRowNum + 1;
        int testCols = 0;

        while (!getCellDataNew(colStartColNum,testCols).equals("")) {
            testCols++;

        }
        Object[][] data = new Object[testRows][1];
        int i = 0;
        for (int rNum = dataStartRowNum; rNum < (dataStartRowNum + testRows); rNum++) {
            HashMap<String, String> table = new HashMap<>();
            for (int cNum = 0; cNum < testCols; cNum++) {
                String colName = getCellDataNew(colStartColNum,cNum);
                String testData = getCellDataNew(rNum,cNum);
                table.put(colName, testData);
            }
            data[i][0] = table;
            i++;
        }
        return data;
    }
    public String getCellDataNew(int rowNum,int colNum) {
        try {
            if (rowNum <= 0)
                return "";

            int index = excelWBook.getSheetIndex(sheetName);

            if (index == -1)
                return "";

            excelWSheet = excelWBook.getSheetAt(index);
            row = excelWSheet.getRow(rowNum - 1);
            if (row == null)
                return "";
            cell = row.getCell(colNum);
            if (cell == null)
                return "";

            if (cell.getCellType() == Cell.CELL_TYPE_STRING)
                return cell.getStringCellValue();
            else if (cell.getCellType() == Cell.CELL_TYPE_NUMERIC || cell.getCellType() == Cell.CELL_TYPE_FORMULA) {

                String cellText = String.valueOf(cell.getNumericCellValue());
                if (HSSFDateUtil.isCellDateFormatted(cell)) {
                    double d = cell.getNumericCellValue();
                    Calendar cal = Calendar.getInstance();
                    cal.setTime(HSSFDateUtil.getJavaDate(d));
                    cellText = (String.valueOf(cal.get(Calendar.YEAR))).substring(2);
                    cellText = cal.get(Calendar.MONTH) + 1 + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cellText;

                }
                return cellText;
            } else if (cell.getCellType() == Cell.CELL_TYPE_BLANK)
                return "";
            else
                return String.valueOf(cell.getBooleanCellValue());
        } catch (Exception e) {

            e.printStackTrace();
            return "row " + rowNum + " or column " + colNum + " does not exist  in xls";
        }
    }
}