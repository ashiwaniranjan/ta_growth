package annotations;

import constants.CommonConstants;
import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
import utils.ExcelUtils;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.lang.reflect.Method;
import java.util.*;
import java.util.stream.Collectors;
/**
 * @author ashiwani
 * @date 14/06/20
 */
public class CDataProvider {
    @DataProvider(name = "ExcelDataProvider",parallel=true)
    public Object[][] getExcelDataProvider(Method method) throws Exception {
        List<String> parameters = Arrays.asList(method.getAnnotation(CDataProvider.FileDetails.class).value());
        Map<String, String> namesMap = parameters.stream().distinct().map(e -> e.split("::"))
                .collect(Collectors.toMap(s -> s[0], s -> s[1]));
        ExcelUtils excelhelper = new ExcelUtils(namesMap.get(CommonConstants.FILENAME),namesMap.get(CommonConstants.SHEETNAME));
        String[][] testObjArray = (String[][]) excelhelper.getTableArray();
        excelhelper.close();
        return (testObjArray);
    }
    @DataProvider(name = "ExcelDataProviderMap",parallel=true)
    public Iterator<Object[]> getExcelDataProviderAsMap(Method method) throws Exception {
        List<String> parameters = Arrays.asList(method.getAnnotation(CDataProvider.FileDetails.class).value());
        Map<String, String> namesMap = parameters.stream().distinct().map(e -> e.split("::"))
                .collect(Collectors.toMap(s -> s[0], s -> s[1]));
        ExcelUtils excelhelper = new ExcelUtils(namesMap.get(CommonConstants.FILENAME),namesMap.get(CommonConstants.SHEETNAME));
        List<Map> mapList=excelhelper.getDataAsMap();
        Collection<Object[]> objects = new ArrayList<>();
        for(Map<String,String> map:mapList){
            objects.add(new Object[]{map});
        }
        excelhelper.close();
        return objects.iterator();
    }
    @DataProvider(parallel=true,name = "DataProviderBasedOnTest")
    public Object[][] getExcelDataOnTest(Method method) throws Exception {
        List<String> parameters = Arrays.asList(method.getAnnotation(CDataProvider.FileDetails.class).value());
        Map<String, String> namesMap = parameters.stream().distinct().map(e -> e.split("::"))
                .collect(Collectors.toMap(s -> s[0], s -> s[1]));
        ExcelUtils excelhelper = new ExcelUtils(namesMap.get(CommonConstants.FILENAME),namesMap.get(CommonConstants.SHEETNAME));
        Object[][] objects=excelhelper.getDataBasedOnTestMethod(method);
        return objects;
    }


    @DataProvider(name = "ExcelDataProviderSingleThreaded")
    public Object[][] getExcelDataProviderSingleThreaded(Method method) throws Exception {
        return getExcelDataProvider(method);
    }

    @DataProvider(name = "ExcelDataProviderMapSingleThread")
    public Iterator<Object[]> getExcelDataProviderAsMapSingleThreaded(Method method) throws Exception {
        return getExcelDataProviderAsMap(method);
    }

    @Retention(java.lang.annotation.RetentionPolicy.RUNTIME)
    @Target({ElementType.METHOD, ElementType.TYPE, ElementType.CONSTRUCTOR})
    public @interface FileDetails {
        String[] value() default {};
    }
    //Iterator<Object>
    @DataProvider(name = "oneway")
    public Iterator<Object> getType1() {
        List list=new ArrayList();
        list.add("hello");
        list.add("london");
        return list.iterator();
    }
    //Object[][]
    @DataProvider(name = "twoway")
    public Object[][] getType2() {
        return new Object[][]{{"hello","kaun"},{"lelo","hm bol rhe"}};
    }
    //Object[]
    @DataProvider(name = "thirdway")
    public Object[] getType3() {
        return new Object[]{"hello","lelllo"};
    }
    //Based on tetMethod
    @DataProvider(name="SearchProvider")
    public Object[][] getDataFromDataprovider(Method m){
        if(m.getName().equalsIgnoreCase("testMethodA")){
            return new Object[][] {
                    { "Guru99", "India" },
                    { "Krishna", "UK" },
                    { "Bhupesh", "USA" }
            };}
        else{
            return new Object[][] {
                    { "Canada" },
                    { "Russia" },
                    { "Japan" }
            };}
    }
    //Based on grouping of test case we can pass data
    @DataProvider(name="SearchProvidernew")
    public Object[][] getDataFromDataprovider(ITestContext c){
        Object[][] groupArray = null;
        for (String group : c.getIncludedGroups()) {
            if(group.equalsIgnoreCase("A")){
                groupArray = new Object[][] {
                        { "Guru99", "India" },
                        { "Krishna", "UK" },
                        { "Bhupesh", "USA" }
                };
                break;
            }
            else if(group.equalsIgnoreCase("B"))
            {
                groupArray = new Object[][] {
                        {  "Canada" },
                        {  "Russia" },
                        {  "Japan" }
                };
            }
            break;
        }
        return groupArray;
    }
}
